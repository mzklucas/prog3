import random
import tkinter
import sqlite3
import random

import Time
import Registro

def janela_registro():
    global lista_times
    #janela_resposta = Registro.Registro(lista_times)
    lista_times.append (Registro.Registro(lista_times))

def grava_times():
    global lista_times
    try:
        con = sqlite3.connect("tutorial.db")
        cur = con.cursor()
    except:
        print("erro ao conectar")
    cur.execute("DELETE FROM times ")
    for i in lista_times:
        cur.execute(f"""
            INSERT INTO times VALUES
            ({i.id}, "{i.nome}", "{i.jogadores}")
        """)
    con.commit()
    con.close()

def carrega_times():
    global lista_times
    lista_times = []
    try:
        con = sqlite3.connect("tutorial.db")
        cur = con.cursor()
    except:
        print("erro ao conectar")
    res = cur.execute("SELECT * FROM times").fetchall()
    print(res)
## Transformar a leitura de jogadores em lista
    for i in iter(res):
        novo_time = Time.Time(i[0], i[1], i[2])
        lista_times.append(novo_time)
    con.close()
    
def sorteia_times():
    global lista_times
    sorteio = lista_times
    if len(sorteio) % 2 == 0:
        random.shuffle(sorteio)
        while len(sorteio) > 0:
            time1 = sorteio.pop()
            time2 = sorteio.pop()
            print (f"Jogo1: {time1.nome} contra {time2.nome}")
    else:
        print("Número ímpar de times!")

def imprime_times():
    for i in lista_times:
        print(i.id)
        print(i.nome)
        print(i.jogadores)
        print ("\n")

if __name__=="__main__":
    global lista_times
    janela_geral = tkinter.Tk()
    lista_times = []

    label_geral = tkinter.Label (janela_geral, text="Janela inicial")
    botao_registra = tkinter.Button(janela_geral, text="Registra", command = janela_registro)
    botao_grava = tkinter.Button(janela_geral, text="Grava", command = grava_times)
    botao_carrega = tkinter.Button(janela_geral, text="Carrega", command = carrega_times)
    botao_sorteia = tkinter.Button(janela_geral, text="Sorteia", command = sorteia_times)
    botao_imprime = tkinter.Button(janela_geral, text="Imprime", command = imprime_times)


    label_geral.grid (row = 0, column = 0)
    botao_registra.grid (row=1, column = 0)
    botao_grava.grid(row=2, column=0)
    botao_carrega.grid(row=3, column=0)
    botao_sorteia.grid(row=4, column=0)
    botao_imprime.grid(row=5, column=0)

    try:
        con = sqlite3.connect("tutorial.db")
        cur = con.cursor()
    except:
        print("erro ao conectar")
    try:
        cur.execute("CREATE TABLE times(id, nome, jogadores)")
    except:
        print("erro ao criar")
    con.commit()
    con.close()


    janela_geral.mainloop()
