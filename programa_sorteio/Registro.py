import tkinter
import Time
import random

class Registro:
    ids_utilizados = []

    def __init__(self, lista_times):
        self.janela = tkinter.Tk()

        self.label_desc_janela = tkinter.Label(self.janela, text = "Janela de inscrição")
        self.label_desc_nome = tkinter.Label(self.janela, text = "Nome do Time")
        self.label_desc_time = tkinter.Label(self.janela, text = "Lista de Jogadores")

        self.entry_nome_time = tkinter.Entry(self.janela )
        self.lista_jogadores =tkinter.Listbox(self.janela)
        self.botao_ok = tkinter.Button(self.janela, text = "Ok", command = lambda: self.finaliza(lista_times))

        self.entry_nome_jogador = tkinter.Entry(self.janela)
        self.botao_cadastra_jogador = tkinter.Button(self.janela, command = self.registra_jogador)


        self.label_desc_janela.grid(row=0, column =0)

        self.label_desc_nome.grid(row=1, column =0)
        self.entry_nome_time.grid (row=1, column=1)


        self.label_desc_time.grid(row=2, column =0)
        self.lista_jogadores.grid(row=2, column=1)

        self.botao_ok.grid(row=3, column =1)

        self.entry_nome_jogador.grid(row=4, column=0)
        self.botao_cadastra_jogador.grid (row=4, column=1)

        self.janela.mainloop()

    
    def registra_jogador(self):
        nome = self.entry_nome_jogador.get()
        if not nome == "": 
            self.lista_jogadores.insert(tkinter.END, nome)
        self.entry_nome_jogador.delete(0,tkinter.END)


    def finaliza(self, lista_times):
        #gera um id da bib random#
        idrandom = random.randint(1,999)
        #checa ver se esse id já esta na lista dos utilizados q eu criei antes#
        while idrandom in Registro.ids_utilizados:
            idrandom = random.randint(1,999)
        Registro.ids_utilizados.append(idrandom)
        #se ele não tiver, ele é printado e usado naquele time#
        print(self.ids_utilizados)
        resposta = Time.Time(idrandom, self.entry_nome_time.get(), self.lista_jogadores.get(0, tkinter.END))
        lista_times.append(resposta)
        self.janela.destroy()
   
